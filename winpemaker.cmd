@echo ON
@echo * 선택해주세요.
@echo   1. Mount 생성
@echo   2. ISO 생성
@echo OFF
set PE_BASEDIR=C:\PE5x64
set MOUNT_DIR=mount
set WORKING_DIR=%PE_BASEDIR%\%MOUNT_DIR%

set /P choice= :
if %choice% == 1 (
	rem mount 삭제하기
	call :mountdir_del %WORKING_DIR%
	goto :eof
	
	rem mount 생성하기
	call :mountdir_make %WORKING_DIR%
	goto :eof
	
	
	
) 

:mountdir_del
	@echo mount directory 삭제중....
	if exist %1\ (
		dism /unmount-image /mountdir:%1 /discard
		echo working directory is %1\...
	)
	
:mountdir_make
	@echo mount directory 생성중....
	if not exist %1\ (
		md %1
	)
	echo boot.wim 마운트...
	REM boot.wim에 있는 파일을 mount에 풀어준다.
	dism /mount-image /imagefile:%PE_BASEDIR%\boot.wim /index:1 /mountdir:%1	
	
	
	
	
	
	
REM   if exist %PE_BASEDIR%\mount
		rem dism /mount-image /imagefile:boot.wim /index:1 /mountdir:mount
REM		@echo %choice%
REM	)
REM	else(
REM		@echo 디렉토리가 존재하지 않습니다
REM	)
REM else if %choice% == 2 (
REM		@echo %choice%
REM	)
