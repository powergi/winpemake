@echo off
setlocal
pushd %~dp0

set MWS32=mount\windows\System32
set MS=media\sources
set MB=media\boot
set ME=media\efi

::IF NOT EXIST FilesToMWS32 goto :eof
IF NOT EXIST %MS%\boot.wim goto :eof
IF NOT EXIST media goto :eof
IF NOT EXIST lp.cab goto :eof
IF NOT EXIST WinPE-FontSupport-KO-KR.cab goto :eof

echo.
echo 	Bios 및 Uefi부팅 모두 가능한 PE 생성
echo.
set /a cnt=0
for /d %%d in (media\*) do (
	IF /i "%%d" neq "%MB%" IF /i "%%d" neq "%ME%" IF /i "%%d" neq "%MS%" rd /s /q %%d && set /a cnt+=1
)
echo 	boot,efi,sources 외 폴더 삭제됨 [총 %cnt% 개]

echo 	원본 boot.wim을 현재경로로 이동
move /y %MS%\boot.wim .\boot.wim

IF EXIST mount rd /q /s mount
md mount
echo.
echo.
echo √boot.wim 이미지 작업 시작
dism /mount-image /imagefile:boot.wim /index:1 /mountdir:mount
echo.
IF not %errorlevel%==0 echo 에러발생&pause >nul&goto :eof
IF EXIST FilesToMWS32\* copy /y FilesToMWS32\*.* %MWS32%
echo.
echo √한글폰트와 언어팩 통합
dism /image:mount /add-package /packagepath:WinPE-FontSupport-KO-KR.cab
dism /image:mount /add-package /packagepath:lp.cab
echo.
echo √한글로케일 설정
dism /image:mount /set-allintl:ko-kr

dism /image:mount /Set-TimeZone:"Korea Standard Time"

::Default 하이브 수정은 명령프롬프트 속성을 일부 편집한 것(폰트, 작업창의 크기 등)
::더블워드 data값은 10진수로 입력한 것
echo.
echo.
echo √Default 하이브 수정 작업(명령프롬프트 옵션 편집)
reg load hku\temp mount\windows\system32\config\default
reg add hku\temp\Console /v screenbuffersize /t reg_dword /d 13107290 /f
reg add hku\temp\Console /v WindowSize /t reg_dword /d 2621530 /f
reg add hku\temp\Console /v historybuffersize /t reg_dword /d 90 /f
reg add hku\temp\Console /v NumberOfHistoryBuffers /t reg_dword /d 10 /f
reg add hku\temp\Console /v HistoryNoDup /t reg_dword /d 1 /f
reg add hku\temp\Console /v FontFamily /t reg_dword /d 54 /f
reg add hku\temp\Console /v FontSize /t reg_dword /d 917504 /f
reg add hku\temp\Console /v FontWeight /t reg_dword /d 400 /f
reg add hku\temp\Console /v FaceName /t reg_sz /d "굴림체" /f
reg unload hku\temp

ping localhost -n 2 >nul
echo √이미지 저장
dism /unmount-image /mountdir:mount /commit
echo.
echo √boot.wim 내보내기
imagex /export boot.wim 1 %MS%\boot.wim
echo.
echo √부팅화면 로케일 한글판 형식으로 설정
bcdedit /store %MB%\BCD /set {bootmgr} locale ko-KR
bcdedit /store %MB%\BCD /set {default} locale ko-KR
del /q .\boot.wim
echo.
echo.
MakeWinPEMedia /iso .\ PE5x64.ISO
pause >nul
exit /b